# Config Service for Microservices with Git

Spring Cloud Config is Spring's client/server approach for storing and serving distributed configurations across multiple applications and environments.